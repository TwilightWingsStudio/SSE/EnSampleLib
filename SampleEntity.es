/* Copyright (c) 2018 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

202
%{
  #include "StdH.h"
%}

class CSampleEntity : CEntity {
name      "CSampleEntity";
thumbnail "Thumbnails\\Marker.tbn";
features  "HasName", "IsTargetable";

properties:

  1 CTString m_strName          "Name" 'N' = "Sample",

resources:

  1 model   MODEL_SAMPLE   "Models\\Editor\\Axis.mdl",
  2 texture TEXTURE_SAMPLE "Models\\Editor\\Vector.tex"

functions:

procedures:

  Main()
  {
    InitAsEditorModel();
    SetPhysicsFlags(0);
    SetCollisionFlags(0);

    // set appearance
    SetModel(MODEL_SAMPLE);
    SetModelMainTexture(TEXTURE_SAMPLE);

    return;
  }
};

